import Head from 'next/head'
import React, { Children } from 'react'
const LayoutPage = ({children,title}) => {
  return (
    <>
        <Head>
       
          <title>{title}</title>
        </Head>
        {children}
        {/* <Footer/> */}
    </>
  )
}

export default LayoutPage