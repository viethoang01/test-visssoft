import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";
import { Skeleton } from "@mui/material";
import background from "@/images/image2.png";

export const ContainerNavBar = styled("div")(({ theme }) => ({
  width: "100%",
  height: 80,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  position: "fixed",
 backgroundColor:"white",
 borderTopColor:"1px solid gray",
 bottom:"0",
  [theme.breakpoints.up("xs")]: {
    display: "flex",
  },
  [theme.breakpoints.up("sm")]: {
    display: "flex",
  },
  [theme.breakpoints.up("md")]: {
    display: "flex",
  },
  [theme.breakpoints.up("lg")]: {
    display: "none",
  },
  [theme.breakpoints.up("xl")]: {
    display: "none",

  },
}));

export const ChatDiv = styled("div")(({ theme }) => ({
    width: 50,
    height: 50,
    display: "flex",
  justifyContent: "center",
  alignItems: "center",
    borderRadius:"50%",
    color:"white",
    backgroundColor:"#022E45",
    fontSize:30,
    cursor:"pointer"
  }));