import React from 'react'
import { NavBarOfOption } from '../Background/sections'
import { IoMdHome } from "react-icons/io";
import { BiCategory } from "react-icons/bi";
import { CiUser } from "react-icons/ci";
import { AiOutlineMenuFold } from "react-icons/ai";
import { IoLogoWechat } from "react-icons/io5";
import { ChatDiv, ContainerNavBar } from './style';
import { Box } from '@mui/material';
import { CenterFlexBox } from '../Header/styles';
export const NavBar = () => {
  return (
    <ContainerNavBar>
        <NavBarOfOption icon={<IoMdHome />} text="Home"/>
        <NavBarOfOption icon={<BiCategory />} text="Categories"/>
        <CenterFlexBox ><ChatDiv><IoLogoWechat /></ChatDiv></CenterFlexBox>
        <NavBarOfOption icon={<CiUser />} text="Profile"/>
        <NavBarOfOption icon={<AiOutlineMenuFold />} text="Menu"/>
    </ContainerNavBar>
  )
}
