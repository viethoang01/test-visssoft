import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
import { Skeleton } from '@mui/material';
export const ContainerCategoryOption = styled('div')(({ theme }) => ({
  width:"100%",
  display:"flex",
  justifyContent:"center",
flexWrap:"wrap",

  
  [theme.breakpoints.up("xs")]: {
    

  },
  [theme.breakpoints.up("sm")]: {
 
  


  },
  [theme.breakpoints.up("md")]: {
   


  },
  [theme.breakpoints.up("lg")]: {

  },
  [theme.breakpoints.up("xl")]: {



  }

  
}));

export const CategoryItem = styled('div')(({ theme }) => ({
    width:"25%",
    height:"50px",
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
    textTransform: "uppercase",
    color:"white",
    fontWeight:"bold",
    cursor:"pointer",
    "&:hover":{
    color:"black",

    },
    [theme.breakpoints.up("xs")]: {
    width:"50%",
      
  
    },
    [theme.breakpoints.up("sm")]: {
   
        width:"50%",
    
  
  
    },
    [theme.breakpoints.up("md")]: {
     
        width:"25%",
  
  
    },
    [theme.breakpoints.up("lg")]: {
        width:"25%",
  
    },
    [theme.breakpoints.up("xl")]: {
        width:"25%",
  
  
  
    }
  
    
  }));