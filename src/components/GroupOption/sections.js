import React from 'react'
import { CategoryItem } from './styles'
import { ButtonShow, ShowProduct } from '../Background/styles'
import { Box } from '@mui/material'

export const CategoryOption = ({title,url,color}) => {
  return (
    <CategoryItem style={{backgroundColor:`${color?color:"back"}`}}>
        {title} 
    </CategoryItem>
  )
}

export const ShowMore = ({icon,text}) => {
    return (
      <ShowProduct>
        <p>Products at crazy cheap price</p>
          <ButtonShow> See more products</ButtonShow>
      </ShowProduct>
    )
  }
