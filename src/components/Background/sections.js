import { Box } from '@mui/material'
import React from 'react'
import { CenterFlexBox, HoverCenterFlexBox } from '../Header/styles'

export const AppOfOption = ({icon,text}) => {
  return (
    <HoverCenterFlexBox style={{flexDirection:"column",alignItems:"center",marginTop:"10px"}}>
        <Box sx={{fontSize:70,color:"gray"}}>
                {icon}
        </Box>
        <Box sx={{    textAlign: "center"}}>
            {text}
        </Box>
    </HoverCenterFlexBox>
  )
}

export const NavBarOfOption = ({icon,text}) => {
    return (
      <HoverCenterFlexBox style={{flexDirection:"column",alignItems:"center",margin:"0 10px"}}>
          <Box sx={{fontSize:30,color:"#022E45"}}>
                  {icon}
          </Box>
          <Box sx={{    textAlign: "center",color:"#022E45"}}>
              {text}
          </Box>
      </HoverCenterFlexBox>
    )
  }

