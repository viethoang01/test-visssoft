import Background from "@/components/Background/background";
import { AppOfOption } from "@/components/Background/sections";
import {
  AdsBackground,
  BottomBackground,
  Brand1ContentBackground,
  Brand2ContentBackground,
  ContainerAdsBackground,

  OtherBackground,
  SaleBox,
  TitleSale,
} from "@/components/Background/styles";
import { CategoryOption, ShowMore } from "@/components/GroupOption/sections";
import { ContainerCategoryOption } from "@/components/GroupOption/styles";
import { Header } from "@/components/Header/sections";
import { CenterFlexBox, ShowMoreAndCenterFlexBox } from "@/components/Header/styles";
import LayoutPage from "@/components/Layout/LayoutPage";
import { NavBar } from "@/components/NabBar/sections";
import { Box } from "@mui/material";
import { useState } from "react";
import { FaWarehouse } from "react-icons/fa";
import { LiaShippingFastSolid } from "react-icons/lia";
import { MdCampaign, MdWifiCalling3 } from "react-icons/md";

const AdsProducts = () => {
  return (
    <ContainerAdsBackground>
    <ShowMoreAndCenterFlexBox style={{flexDirection:"column", }}>
      <AdsBackground>
        <CenterFlexBox style={{ justifyContent: "end", width: "50%" }}>
          <Box>We</Box>
          <Brand1ContentBackground style={{ margin: "0 20px 0 5px" }}>
            <Box>Beat all</Box>
          </Brand1ContentBackground>
        </CenterFlexBox>
        <CenterFlexBox
          style={{
            justifyContent: "start",
            alignItems: "flex-start",
            width: "50%",
            flexDirection: "column",
          }}
        >
          <Box>Full retail prices </Box>
          <Brand2ContentBackground>
            same day DELIVERY
          </Brand2ContentBackground>
        </CenterFlexBox>
      </AdsBackground>
      <ShowMore />
    </ShowMoreAndCenterFlexBox>
    <OtherBackground>
      <AppOfOption
        icon={<LiaShippingFastSolid />}
        text="SAME DAY DELIVERY"
      />
      <AppOfOption icon={<FaWarehouse />} text="WAREHOUSE DIRECT SALE" />
      <AppOfOption icon={<MdCampaign />} text="OPEN 7 DAYS" />
      <AppOfOption icon={<MdWifiCalling3 />} text="CONNECT 0988903545" />
    </OtherBackground>
  </ContainerAdsBackground>
  )
}

export default AdsProducts