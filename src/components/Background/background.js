import { AppOfOption } from "@/components/Background/sections";
import {
  AdsBackground,
  BottomBackground,
  Brand1ContentBackground,
  Brand2ContentBackground,
  BrandContentBackground,
  ButtonBackground,
  ContainerAdsBackground,
  ContainerBackground,
  ContextSale,
  Item2SaleBox,
  ItemSaleBox,
  LeftContentBackground,
  OtherBackground,
  SaleBox,
  TitleSale,
} from "@/components/Background/styles";
import { CategoryOption, ShowMore } from "@/components/GroupOption/sections";
import { ContainerCategoryOption } from "@/components/GroupOption/styles";
import { Header } from "@/components/Header/sections";
import { CenterFlexBox, ShowMoreAndCenterFlexBox } from "@/components/Header/styles";
import LayoutPage from "@/components/Layout/LayoutPage";
import { NavBar } from "@/components/NabBar/sections";
import { Box } from "@mui/material";
import { useState } from "react";
import { FaWarehouse } from "react-icons/fa";
import { LiaShippingFastSolid } from "react-icons/lia";
import { MdCampaign, MdWifiCalling3 } from "react-icons/md";

const Background = () => {
  return (
    <ContainerBackground>
        <SaleBox>
          <Item2SaleBox />
          <p>Sale!</p>
          <ItemSaleBox />
        </SaleBox>
        <BottomBackground>
          <LeftContentBackground>
            <BrandContentBackground> SAVE $200</BrandContentBackground>
            <TitleSale>$199</TitleSale>
            <ContextSale>Sofa 3 seats</ContextSale>
          </LeftContentBackground>
          <ButtonBackground> Shop now</ButtonBackground>
        </BottomBackground>
      </ContainerBackground>
  )
}



export default Background