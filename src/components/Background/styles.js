import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";
import { Skeleton } from "@mui/material";
import background from "@/images/image2.png";

export const ContainerBackground = styled("div")(({ theme }) => ({
  width: "100%",
  height: "800px",
  backgroundImage: `url( ${background.src})`,
  backgroundSize: "cover",
  position: "absolute",
  [theme.breakpoints.up("xs")]: {
    height: "300px",
  },
  [theme.breakpoints.up("sm")]: {
    height: "300px",
  },
  [theme.breakpoints.up("md")]: {
    height: "300px",
  },
  [theme.breakpoints.up("lg")]: {
    height: "800px",
  },
  [theme.breakpoints.up("xl")]: {
    height: "800px",
  },
}));

export const SaleBox = styled("div")(({ theme }) => ({
  width: "300px",
  height: "150px",
  fontSize: "72px",
  backgroundColor: "white",
  borderBottomRightRadius: "500px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  textTransform: "uppercase",
  color: "#FF4C26",
  fontWeight: "bold",
  fontFamily: "cursive",
  position: "relative",

  [theme.breakpoints.up("xs")]: {
    width: "170px",
    height: "60px",
    fontSize: "46px",
  },
  [theme.breakpoints.up("sm")]: {
    width: "170px",
    height: "60px",
    fontSize: "46px",
  },
  [theme.breakpoints.up("md")]: {
    width: "170px",
    height: "60px",
    fontSize: "46px",
  },
  [theme.breakpoints.up("lg")]: {
    width: "300px",
    height: "100px",
    fontSize: "72px",
  },
  [theme.breakpoints.up("xl")]: {
    width: "300px",
    height: "100px",
    fontSize: "72px",
  },
}));

export const ItemSaleBox = styled("div")(({ theme }) => ({
  width: "30px",
  height: "30px",
  borderRadius: "50%",
  backgroundColor: "#2099B6",
  position: "absolute",
  top: "0",
  right: "0",
  marginRight: "-15px",
  marginTop: "5px",
  //   marginLeft: "20px",
  fontFamily: "cursive",
}));

export const Item2SaleBox = styled("div")(({ theme }) => ({
  width: "30px",
  height: "30px",
  borderRadius: "50%",
  backgroundColor: "#F9B03C",
  position: "absolute",
  bottom: "0",
  left: "0",

  marginBottom: "-15px",
  marginLeft: "-15px",
}));
export const LeftContentBackground = styled("div")(({ theme }) => ({
  width: "230px",
  height: "180px",
  color: "white",
  display: "flex",
  justifyContent: "center",
  alignItems: "start",
  flexDirection: "column",
}));

export const BrandContentBackground = styled("div")(({ theme }) => ({
  width: "130px",
  height: "40px",
  color: "white",
  borderBottomRightRadius: "500px",
  borderTopLeftRadius: "500px",
  backgroundColor: "#FF4C26",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  textTransform: "uppercase",
}));

export const Brand1ContentBackground = styled("div")(({ theme }) => ({
  width: "130px",
  height: "40px",
  color: "white",
  borderBottomRightRadius: "500px",
  borderTopLeftRadius: "500px",
  backgroundColor: "#FF4C26",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  textTransform: "uppercase",
  [theme.breakpoints.up("xs")]: {
    width: "100px",
    height: "30px",
},
  [theme.breakpoints.up("sm")]: {
    width: "100px",
    height: "30px",
  },
  [theme.breakpoints.up("md")]: {
    width: "100px",
    height: "30px",
  },
  [theme.breakpoints.up("lg")]: {
    width: "130px",
    height: "40px",
},
  [theme.breakpoints.up("xl")]: {
    width: "130px",
    height: "40px",
},
}));

export const Brand2ContentBackground = styled("div")(({ theme }) => ({
  width: "330px",
  height: "25px",
  color: "#022E45",
  borderBottomRightRadius: "500px",
  borderTopLeftRadius: "500px",
  backgroundColor: "#F9B03C",
  display: "flex",
  justifyContent: "end",
  alignItems: "center",
  textTransform: "uppercase",
  [theme.breakpoints.up("xs")]: {
    width: "130px",
    fontSize: "10px",
},
  [theme.breakpoints.up("sm")]: {
    width: "130px",
    fontSize: "10px",
  },
  [theme.breakpoints.up("md")]: {
    width: "130px",
    fontSize: "10px",
  },
  [theme.breakpoints.up("lg")]: {
    width: "330px",
    fontSize: "14px",
},
  [theme.breakpoints.up("xl")]: {
    width: "330px",
    fontSize: "14px",
},
}));

export const BottomBackground = styled("div")(({ theme }) => ({
  width: "100%",
  padding: "0 20px",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "flex-end",
  position: "absolute",
  bottom: 0,
  fontFamily: "cursive",
  [theme.breakpoints.up("xs")]: {
    justifyContent: "space-between",
  },
  [theme.breakpoints.up("sm")]: {
    justifyContent: "space-between",
  },
  [theme.breakpoints.up("md")]: {
    justifyContent: "space-between",
  },
  [theme.breakpoints.up("lg")]: {
    justifyContent: "space-around",
  },
  [theme.breakpoints.up("xl")]: {
    justifyContent: "space-around",
  },
}));

export const ButtonBackground = styled("button")(({ theme }) => ({
  width: "200px",
  height: "50px",
  color: "white",
  backgroundColor: "#F9B03C",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  textTransform: "uppercase",
  borderRadius: "8px",
  fontWeight: "bold",
  marginBottom: "20px",
  "&:hover": {
    backgroundColor: "#FFD073",
    color: "gray",
  },
}));

export const TitleSale = styled("p")(({ theme }) => ({
  marginTop: "-20px",
  fontWieght: "bold",
  [theme.breakpoints.up("xs")]: {
    fontSize: "46px",
  },
  [theme.breakpoints.up("sm")]: {
    fontSize: "46px",
  },
  [theme.breakpoints.up("md")]: {
    fontSize: "46px",
  },
  [theme.breakpoints.up("lg")]: {
    fontSize: "72px",
  },
  [theme.breakpoints.up("xl")]: {
    fontSize: "72px",
  },
}));

export const ContextSale = styled("p")(({ theme }) => ({
  // marginTop:"-25px",
  fontWieght: "bold",
  [theme.breakpoints.up("xs")]: {
    fontSize: "18px",
  },
  [theme.breakpoints.up("sm")]: {
    fontSize: "18px",
  },
  [theme.breakpoints.up("md")]: {
    fontSize: "18px",
  },
  [theme.breakpoints.up("lg")]: {
    fontSize: "18px",
  },
  [theme.breakpoints.up("xl")]: {
    fontSize: "18px",
  },
}));

export const AdsBackground = styled("div")(({ theme }) => ({
  backgroundColor: "#022E45",
  width: "100%",
  height: "120px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: "8px",
  color: "white",
  fontSize: "24px",
  textTransform: "uppercase",
  fontWeight: "bold",
  fontFamily: "cursive",
  [theme.breakpoints.up("xs")]: {
    fontSize: "14px",
  },
  [theme.breakpoints.up("sm")]: {
    fontSize: "14px",
  },
  [theme.breakpoints.up("md")]: {
    fontSize: "14px",
  },
  [theme.breakpoints.up("lg")]: {
    fontSize: "24px",
  },
  [theme.breakpoints.up("xl")]: {
    fontSize: "24px",
  },
}));

export const OtherBackground = styled("div")(({ theme }) => ({
  fontWeight: "bold",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: "20%",
  [theme.breakpoints.up("xs")]: {
    width: "95%",
    flexDirection: "row",
  },
  [theme.breakpoints.up("sm")]: {
    width: "95%",
    flexDirection: "row",
  },
  [theme.breakpoints.up("md")]: {
    width: "95%",
    flexDirection: "row",
  },
  [theme.breakpoints.up("lg")]: {
    width: "20%",
    flexDirection: "column",
  },
  [theme.breakpoints.up("xl")]: {
    width: "20%",
    flexDirection: "column",
  },
}));

export const ContainerAdsBackground = styled("div")(({ theme }) => ({
  width: "100%",
  display: "flex",
  justifyContent: "space-around",
  alignItems: "flex-start",
  fontWeight: "bold",
marginBottom:"90px",
  [theme.breakpoints.up("xs")]: {
    marginTop: "330px",
    flexDirection: "column",
    alignItems: "center",
  },
  [theme.breakpoints.up("sm")]: {
    marginTop: "330px",
    flexDirection: "column",
    alignItems: "center",
  },
  [theme.breakpoints.up("md")]: {
    marginTop: "330px",
    flexDirection: "column",
    alignItems: "center",
  },
  [theme.breakpoints.up("lg")]: {
    marginTop: "900px",
    flexDirection: "row",
    alignItems: "flex-start",
  },
  [theme.breakpoints.up("xl")]: {
    marginTop: "900px",
    flexDirection: "row",
    alignItems: "flex-start",
  },
}));
export const ButtonShow = styled("button")(({ theme }) => ({
    width: "300px",
    height: "30px",
    color: "white",
    backgroundColor: "#022E45",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textTransform: "uppercase",
    borderRadius: "8px",
    fontWeight: "bold",
    "&:hover": {
    //   backgroundColor: "gray",
      color: "gray",
    },
  }));
  export const ShowProduct = styled("div")(({ theme }) => ({
    width: "100%",
    height:"100px",
    display: "flex",
    justifyContent: "space-between",
    alignItems:"center",
    textTransform: "uppercase",
     fontWeight: "bold",
  marginTop:"30px",
  color:"#022E45",
  borderRadius:"8px",
  padding:"10px",
    backgroundColor:"white",
    [theme.breakpoints.up("xs")]: {
        fontSize: "14px",
        display: "none",
      },
      [theme.breakpoints.up("sm")]: {
        fontSize: "14px",
        display: "none",
      },
      [theme.breakpoints.up("md")]: {
        fontSize: "14px",
        display: "none",
      },
      [theme.breakpoints.up("lg")]: {
        fontSize: "14px",
        display: "flex",
      },
      [theme.breakpoints.up("xl")]: {
        fontSize: "14px",
        display: "flex",
      },
  }));
  