import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";
import { Skeleton } from "@mui/material";
export const ContainerHeader = styled("div")(({ theme }) => ({
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: "white",
  padding: "10px 0",
  [theme.breakpoints.up("xs")]: {
    justifyContent: "space-between",
    alignItems: "flex-start",
  },
  [theme.breakpoints.up("sm")]: {
    justifyContent: "space-between",
    alignItems: "flex-start",
  },
  [theme.breakpoints.up("md")]: {
    justifyContent: "center",
    alignItems: "center",
  },
  [theme.breakpoints.up("lg")]: {
    justifyContent: "center",
    alignItems: "center",
  },
  [theme.breakpoints.up("xl")]: {
    justifyContent: "center",
    alignItems: "center",
  },
}));

export const LeftHeader = styled("div")(({ theme }) => ({
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "row",

  [theme.breakpoints.up("xs")]: {
    flexDirection: "column",
    alignItems: "flex-start",
    paddingLeft: "20px",
    paddingBottom: "10px",
  },
  [theme.breakpoints.up("sm")]: {
    flexDirection: "column",
    alignItems: "flex-start",
    paddingLeft: "20px",
    paddingBottom: "10px",
  },
  [theme.breakpoints.up("md")]: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: "0",
  },
  [theme.breakpoints.up("lg")]: {
    flexDirection: "row",
    alignItems: "center",
    padding: "0",
  },
  [theme.breakpoints.up("xl")]: {
    flexDirection: "row",
    alignItems: "center",
    padding: "0",
  },
}));

export const RightHeader = styled("div")(({ theme }) => ({
  width: "50%",
  display: "flex",
  justifyContent: "center",

  [theme.breakpoints.up("xs")]: {
    flexDirection: "column",
    alignItems: "flex-end",
    padding: "20px 20px 0 0",
  },
  [theme.breakpoints.up("sm")]: {
    flexDirection: "column",
    alignItems: "flex-end",
    padding: "20px 20px 0 0",
  },
  [theme.breakpoints.up("md")]: {
    flexDirection: "row",
    alignItems: "center",
    padding: "0",
  },
  [theme.breakpoints.up("lg")]: {
    flexDirection: "row",
    alignItems: "center",
    padding: "0",
  },
  [theme.breakpoints.up("xl")]: {
    flexDirection: "row",
    alignItems: "center",
    paddingRight: "0",
  },
}));

export const LogoHeader = styled("img")(({ theme }) => ({
  width: "100px",
  height: "50px",
}));

export const CenterFlexBox = styled("div")(({ theme }) => ({
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
}));

export const ShowMoreAndCenterFlexBox = styled("div")(({ theme }) => ({
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  [theme.breakpoints.up("xs")]: {
    width: "95%",
    fontSize: "14px",
  },
  [theme.breakpoints.up("sm")]: {
    width: "95%",
    fontSize: "14px",
  },
  [theme.breakpoints.up("md")]: {
    width: "95%",
    fontSize: "14px",
  },
  [theme.breakpoints.up("lg")]: {
    width: "65%",
    fontSize: "24px",
  },
  [theme.breakpoints.up("xl")]: {
    width: "65%",
    fontSize: "24px",
  },
}));
export const HoverCenterFlexBox = styled("div")(({ theme }) => ({
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  padding: "5px",
  "&:hover": {
    backgroundColor: "#D9D9D9",
  },
}));

export const ContainerSearch = styled("div")(({ theme }) => ({
  width: "600px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  border: "1px solid  gray",
  borderRadius: "25px",
  padding: "2px 3px",
  [theme.breakpoints.up("xs")]: {
    width: "250px",
  },
  [theme.breakpoints.up("sm")]: {
    width: "250px",
  },
  [theme.breakpoints.up("md")]: {
    width: "300px",
  },
  [theme.breakpoints.up("lg")]: {
    width: "600px",
  },
  [theme.breakpoints.up("xl")]: {
    width: "600px",
  },
}));

export const InputSearch = styled("input")(({ theme }) => ({
  width: "93%",
  border: "none",
  backgroundColor: "inherit",
  padding: "5px 10px",
  "&:focus": {
    outlineWidth: 0,
  },
}));
