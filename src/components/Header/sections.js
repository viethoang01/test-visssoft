import React from "react";
import logo from "@/images/logo.png";
import { FaGift, FaSearch } from "react-icons/fa";
import {
  CenterFlexBox,
  ContainerHeader,
  ContainerSearch,
  InputSearch,
  LeftHeader,
  LogoHeader,
  RightHeader,
} from "./styles";
export const Header = () => {
  return (
    <ContainerHeader>
      
        <LeftHeader>
          
            <LogoHeader src={`${logo.src}`} />
            <Search/>
        </LeftHeader>

        <RightHeader>
            <FaGift style={{fontSize:"24px"}}/>
        </RightHeader>
     
    </ContainerHeader>
  );
};

export const Search = () => {
    return (
      <ContainerSearch>
        <InputSearch type="text" placeholder="Search ..."/>
        <FaSearch style={{cursor:"pointer"}}/>
      </ContainerSearch>
    );
  };


