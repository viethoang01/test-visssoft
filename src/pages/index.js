import AdsProducts from "@/components/Background/AdsProducts";
import Background from "@/components/Background/background";
import { AppOfOption } from "@/components/Background/sections";
import {
  AdsBackground,
  BottomBackground,
  Brand1ContentBackground,
  Brand2ContentBackground,
  BrandContentBackground,
  ButtonBackground,
  ContainerAdsBackground,
  ContainerBackground,
  ContextSale,
  Item2SaleBox,
  ItemSaleBox,
  LeftContentBackground,
  OtherBackground,
  SaleBox,
  TitleSale,
} from "@/components/Background/styles";
import { CategoryOption, ShowMore } from "@/components/GroupOption/sections";
import { ContainerCategoryOption } from "@/components/GroupOption/styles";
import { Header } from "@/components/Header/sections";
import { CenterFlexBox, ShowMoreAndCenterFlexBox } from "@/components/Header/styles";
import LayoutPage from "@/components/Layout/LayoutPage";
import { NavBar } from "@/components/NabBar/sections";
import { Box } from "@mui/material";
import { useState } from "react";
import { FaWarehouse } from "react-icons/fa";
import { LiaShippingFastSolid } from "react-icons/lia";
import { MdCampaign, MdWifiCalling3 } from "react-icons/md";

export default function Home() {
  const [categorys, setCategorys] = useState([
    "livingroom",
    "kitchen",
    "bedroom",
    "bathroom",
  ]);
  const mapColors = {
    livingroom: "#F9B03C",
    kitchen: "#FF4C26",
    bedroom: "#022E45",
    bathroom: "#2099B6",
  };

  return (
    <LayoutPage title="Home page">
      <Header />
      <ContainerCategoryOption>
        {categorys.map((item, index) => (
          <CategoryOption key={index} title={item} color={mapColors[item]}>
            {mapColors[item]}
          </CategoryOption>
        ))}
      </ContainerCategoryOption>

      <Background/>

     <AdsProducts/>
      <NavBar />
    </LayoutPage>
  );
}
